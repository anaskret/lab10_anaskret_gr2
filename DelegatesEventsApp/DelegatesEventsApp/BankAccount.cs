﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesEventsApp
{
    public class BankAccount
    {
        public Guid AccountNumber { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public BankAccount(string email, string firstName, string lastName)
        {
            Email = email;
            FirstName = firstName;
            LastName = lastName;
        }

        public override string ToString()
        {
            return $"First name: {FirstName}, Last name: {LastName},\n email: {Email}";
        }
    }
   /* public interface IAccountManager
    {
        event EventHandler<BankAccount> AccountCreated;
    }

    public interface IEmailManager
    {
        void Send(string email);
    }*/
}
