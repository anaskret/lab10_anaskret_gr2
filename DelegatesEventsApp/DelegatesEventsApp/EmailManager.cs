﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesEventsApp
{
    class EmailManager
    {
        public string Send(string email)
        {
            return $"Email has been sent to: {email}";
        }
    }
}
