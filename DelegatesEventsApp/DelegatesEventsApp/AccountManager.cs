﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesEventsApp
{
    public class AccountManager
    {
        private readonly AccountCreator creator;

        public event AccountEventHandler AccountCreated;
        public AccountManager(AccountCreator creator)
        {
            this.creator = creator;
        }
        public void Add()
        {
            Console.Write("First name: ");
            string firstName = Console.ReadLine();
            Console.Write("Last name: ");
            string lastName = Console.ReadLine();
            Console.Write("Email: ");
            string email = Console.ReadLine();
            var account = creator(email, firstName, lastName);
            AccountCreated(account);
        }
    }

    public delegate BankAccount AccountCreator(string email, string firstName, string lastName);
    public delegate void AccountEventHandler(BankAccount account);
}
