﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DelegatesEventsApp
{
    class Program
    {
        private static List<BankAccount> accounts = new List<BankAccount>();
        //private static bool works = true;

        static void Main(string[] args)
        {
            var creator = new AccountCreator(AddAccountToList);
            var manager = new AccountManager(AddAccountToList);
            manager.AccountCreated += Manager_AccountCreated;
            manager.AccountCreated += EmailShouldBeSent_AccountCreated;
            while (true)
            {
                manager.Add();
                var end = Console.ReadLine();
                if (end == "a")
                    break;
            }
            accounts.ForEach(item => Console.WriteLine(item.ToString()));
            Console.ReadKey();
        }

        private static void EmailShouldBeSent_AccountCreated(BankAccount account)
        {
            var email = new EmailManager();
            if (!string.IsNullOrWhiteSpace(account.Email))
                Console.WriteLine($"{email.Send(account.Email)}");
            else Console.WriteLine("Wrong email");
        }

        private static void Manager_AccountCreated(BankAccount account)
        {
            Console.WriteLine("Account has been created");
        }

        private static BankAccount AddAccountToList(string email,string firstName, string lastName)
        {
            var account = new BankAccount(email, firstName, lastName);
            accounts.Add(account);
            return account;
        }
    }
}
