﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_Events
{
    class Program
    {
        static void Main(string[] args)
        {
            DelegateClass manager = new DelegateClass();
            manager.MyEvent += Manager_MyEvent;
            Console.WriteLine($"Result of add: {manager.Add(10, 4)}");
            Console.ReadKey();
            /*DelegateClass.DelegateInt adding = new DelegateClass.DelegateInt(manager.Add);
            int addResult = adding(5, 4);
            addResult.ToString();
            Console.WriteLine("asd");*/

        }

        private static void Manager_MyEvent(int number)
        {
            Console.WriteLine($"The add result is: {number.ToString()}");
        }
    }
}
