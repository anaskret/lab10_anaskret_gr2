﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate_Events
{
    class DelegateClass
    {
        public delegate void DelegateInt(int number);

        public event DelegateInt MyEvent;

        public int Add(int a, int b)
        {
            return a + b;
        }
        public int Sub(int a, int b)
        {
            return a - b;
        }
        public int Mul(int a, int b)
        {
            return a * b;
        }
        public int Div(int a, int b)
        {
            if (b != 0)
                return a / b;
            else return 0;
        }
        class KeyboardMonitor
        {
            public event EventHandler Click;
        }
    }
}
